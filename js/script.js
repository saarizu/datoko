// Initialize cart
if (localStorage.getItem('cart') === null)
    localStorage.setItem('cart', '[]');

/**
* Contact via WhatsApp
*/
$('#contact-form').find('button').click(function () {
    let $form = $('#contact-form'),
        $name = $form.find('#name'),
        $message = $form.find('#message'),
        $text = $form.find('#text'),
        message = 'Halo, nama saya ' + $name.val() + decodeURIComponent('%0A%0A') + $message.val();

    if ($name.val() && $message.val()) {
        $text.val(message);
        $name.removeAttr('name');
        $message.removeAttr('name');
        $form.submit();
    }
})

$(window).on('scroll', function() {
    stickyNavbar();
})

$(document).on('ready', function() {
    stickyNavbar();
})

let navbar = document.querySelector(".navbar--desktop"),
    sticky = navbar.offsetTop;

function stickyNavbar() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky");
        $('body').css('margin-top', $(navbar).height() + 'px');
    } else {
        navbar.classList.remove("sticky");
        $('body').css('margin-top', 0);
    }
}

$('.toggle-search').click(function() {
    let $inputSearch = $('input.search');

    if ($inputSearch.hasClass('w-100')) {
        $inputSearch.parent().submit();
        $inputSearch.unbind('blur');        
    } else {
        $inputSearch.addClass('w-100');
        $inputSearch.focus();
    }
})