onUpdate();

function onUpdate() {
    let cart = JSON.parse(localStorage.getItem('cart')),
        html = ``;
    
    if (cart === null || cart.length === 0)
        html += `
            <tr>
                <td colspan="6" class="text-center text-muted bg-light">Tidak ada produk.</td>
            </tr>
        `

    $.each(cart, function(index, item) {
        let itemVariances = item.variance
    
        html += `
            <tr class="cart-product">
                <td style="width: 130px;">
                    <img src="${item.thumbnail}">
                </td>
                <td>
                    <h2 class="h6 font-weight-bold mb-1">${item.name}</h2>`
                    let printVariances = [];
    
                    for (const variance in itemVariances) {
                        printVariances.push(`<strong>${variance}</strong>: ${itemVariances[variance]}`)
                    }
    
                    html += `<div class="text-muted">${printVariances.join('<br>')}</div>`
        html += `
                </td>
                <td>${new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(item.price).split(',')[0]}</td>
                <td>
                    <div class="input-number input-group col mb-3 flex-grow-0" style="max-width: 150px; min-width: 150px">
                        <div class="input-group-prepend">
                            <button class="btn btn-light" type="button" onclick="onChange('subtract', ${index})" ${item.quantity === 1 ? 'style="cursor: not-allowed;" disabled' : ''}>-</button>
                        </div>
                        <input name="quantity" type="text" class="form-control bg-light border-0" value="${item.quantity}" min="1">
                        <div class="input-group-append">
                            <button class="btn btn-light" type="button" onclick="onChange('add', ${index})">+</button>
                        </div>
                    </div>                
                </td>
                <td>${new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(item.price*item.quantity).split(',')[0]}</td>
                <td>
                    <button class="btn btn-sm btn-danger" onclick="onDelete(${index})">
                        <i class="las la-trash"></i>
                    </button>
                </td>
            </tr>
        `
    });
    
    $('#products').html(html);

    // update table footer
    let newCart = JSON.parse(localStorage.getItem('cart'))
    let totalPrice = 0

    newCart.forEach((product) => {
        totalPrice += product.price * product.quantity
    })

    let totalHTML = `
        <tr class="bg-light font-weight-bold">
            <td colspan="4">Harga Total</td>
            <td colspan="2">${new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(totalPrice).split(',')[0]}</td>
        </tr>
    `
    $('#total').html(totalHTML)
}

function onSendMessage() {
    let url = 'https://api.whatsapp.com/send?phone=%2B6289605554921&text=',
        cart = JSON.parse(localStorage.getItem('cart')),
        recipient = $('#details').find('#recipient').val(),
        address = $('#details').find('#address').val();
    
    if (recipient === '' || address === '' || typeof cart === 'undefined' || cart.length === 0) {
        alert('Pastikan Anda sudah mengisi kolom "nama penerima" dan "alamat lengkap", serta keranjang Anda tidak kosong.')
        window.location = window.location.href.replace('#', '')
    } else {
        url += `*Penerima:* ${recipient} %0A%0A*Alamat:* ${address} %0A%0A *Pesanan:* %0A`
    
        $.each(cart, function (index, product) {
            url+= `- (${product.quantity}) ${product.name}%0A`
        });
    
        window.open(url)

        localStorage.setItem('cart', '[]')
    }
}

function onCheckout() {
    $('#details').modal();
}

function onDelete(index) {
    if (confirm("Apakah Anda yakin ingin menghapus produk ini?")) {
        let cart = JSON.parse(localStorage.getItem('cart'));
    
        cart.splice(index, 1);
    
        localStorage.setItem('cart', JSON.stringify(cart));
    
        onUpdate();    
    }
}

function onChange(type, index) {
    let cart = JSON.parse(localStorage.getItem('cart'));

    switch (type) {
        case 'add':
            cart[index].quantity++
            break;
        case 'subtract':
            cart[index].quantity--
            break;
    
        default:
            alert('waw')
            break;
    }

    localStorage.setItem('cart', JSON.stringify(cart));

    onUpdate();
}