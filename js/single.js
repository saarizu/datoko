let $product_imgs = $('.product-images'),
    $img = $('.description img'),
    $img_clone = $img.clone(),
    $variances = $('[class^=v-]'),
    variances = {};

// $('.atc').data('price', )

if ($variances.length > 0) {
    $('<div class="variance text-muted my-4 p-3 bg-light shadow-sm"></div>').insertBefore($('.description'));

    const $newVariances = $('.variance')

    $variances.each((i, variance) => {
        
        let name = $(variance).attr('class').split('v-')[1].split(':')[0];

        if (typeof variances[name] === 'undefined')
            variances[name] = []

        variances[name].push($(variance).attr('class').split(':')[1])
    })

    for (const key in variances) {
        if (variances.hasOwnProperty(key)) {

            const element = variances[key];

            let variance = `
                <div class="row mb-3">
                    <div class="col flex-grow-0">
                        <strong class="variance-key">${key}:</strong>
                    </div>
                    <div class="col">`
                        element.forEach((value) => {
                            variance += `
                                <button class="btn btn-sm btn-outline-secondary px-4 mr-2" data-key="${key}">
                                    ${value}
                                </button>
                            `
                        })
                        variance +=
                    `</div>
                </div>`

            $newVariances.append($(variance))
        }
    }

    /**
     * ATC Variance
     * 
     */
    let selectedVariance = {};

    $('.variance button').click(function () {
        $(this).removeClass('btn-outline-secondary').addClass(['btn-secondary', 'selected']);
        $(this).siblings().removeClass(['btn-secondary', 'selected']).addClass('btn-outline-secondary');

        $('.variance .btn.selected').each(function () {
            selectedVariance[$(this).data('key')] = $(this).text().trim();
        });

        $('.atc').data('variance', selectedVariance);
    });
} else {
    $('<div class="variance text-muted my-4 p-3 bg-light shadow-sm d-none"></div>').insertBefore($('.description'));
}

$img.removeAttr('src').remove();
$product_imgs.children().remove();

$img_clone.each(function () {
    let product = document.createElement('div'),
        img = document.createElement('img');

    product.setAttribute('class', 'product');
    img.src = 'data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22569%22%20height%3D%22500%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20569%20500%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_170fc87b270%20text%20%7B%20fill%3A%23AAAAAA%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A28pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_170fc87b270%22%3E%3Crect%20width%3D%22569%22%20height%3D%22500%22%20fill%3D%22%23EEEEEE%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22196.359375%22%20y%3D%22262.6%22%3ELoading...%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E';
    img.dataset.src = $(this).attr('src');

    product.appendChild(img);

    $product_imgs.append(product);
});

new LazyLoad({
    elements_selector: ".product-images img"
});

let owl = $('.owl-carousel');

owl.on('changed.owl.carousel', function (event) {
    let current_image = $('.current-image'),
        current_item = $('.owl-item')[event.item.index];

    $(current_item).addClass('current');
    $(current_item).siblings().removeClass('current');

    if (typeof current_item != 'undefined') {
        current_image.attr('src', $(current_item).find('img').attr('data-src'));
    } else {
        current_image.attr('src', $('.product').first().find('img').attr('data-src'));
    }
});

owl.owlCarousel({
    items: 6,
    loop: false,
    center: true,
    margin: 15
});

$('.owl-item').click(function () {
    owl.trigger("to.owl.carousel", [$(this).index(), 1000]);
})

/**
 * Add to Cart
 *
 * The element which applied, need to have these attributes:
 *   data-id
 *   data-thumbnail
 *   data-name
 *   data-price
 *   data-quantity
 */
$('.atc').click(function () {

    const product = $('.atc').data()

    let cart = JSON.parse(localStorage.getItem('cart'))

    if ($variances.length === 0 || varianceCheck()) {

        let productIsExists = false

        cart.forEach((cartProduct) => {

            // check if the products are the same
            let _product = _.pick(product, ['id', 'variance'])
            let _cartProduct = _.pick(cartProduct, ['id', 'variance'])

            // add product's quantity if products are the same
            if (_.isEqual(_product, _cartProduct)) {

                productIsExists = true;

                cartProduct.quantity += product.quantity

                localStorage.setItem('cart', JSON.stringify(cart))

                return
            }
        })

        if (!productIsExists) {
            addProductToCart()
        }

        showModal();
    } else {

        alert("Silakan isi variasi produk!");
    }

})

let $addQty = $('#addQty')
let $subtractQty = $('#subtractQty')
let $qty = $('.input-number input')
let qty = Number($qty.val());

$addQty.click(function () {
    qty++;

    $qty.val(qty)

    updateAtcQuantity()
})

$subtractQty.click(function () {
    if (qty > 1) {
        qty--
        $qty.val(qty)
    }

    updateAtcQuantity();
})

$qty.blur(function () {
    qty = Number($qty.val())

    if (qty > 1) {
        $subtractQty.css({ cursor: 'pointer' }).removeAttr('disabled')
    }

    updateAtcQuantity()
})

function updateAtcQuantity() {
    $('.atc').data('quantity', qty);

    if (qty === 1) {
        $subtractQty.css({ cursor: 'not-allowed' }).attr('disabled', '');
    } else if (qty === 2) {
        $subtractQty.css({ cursor: 'pointer' }).removeAttr('disabled')
    }
}

let showModal = function showATCSuccessModal() {
    const product = $('.atc').data()
    const cart = JSON.parse(localStorage.getItem('cart'))

    let totalCartPrice = 0
    let totalCartProducts = 0

    cart.forEach((product) => {
        totalCartPrice += product.quantity * product.price
        totalCartProducts += product.quantity
    })

    $('#atcImg').attr('src', product.thumbnail);
    $('#atcName').text(product.name);

    $('#atcVariance').text(_.values(product.variance).join(', '));
    $('#atcQuantity').text(product.quantity);

    $('#cartProductsCount').text(totalCartProducts);
    $('#totalCartPrice').text(new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(totalCartPrice).split(',')[0]);

    $('#atcModal').modal();
}

function addProductToCart() {
    const product = $('.atc').data()

    let cart = JSON.parse(localStorage.getItem('cart'))

    cart.push({
        id: product.id,
        name: product.name,
        thumbnail: product.thumbnail,
        price: product.price,
        quantity: Number(product.quantity),
        variance: product.variance
    })

    localStorage.setItem('cart', JSON.stringify(cart))
}

function varianceCheck() {
    const varianceCount = $('.variance > *').length
    const atcVariance = $('.atc').data('variance')

    if ($('.variance')) {
        if (typeof atcVariance === 'undefined' || varianceCount !== _.keys(atcVariance).length) {
            const $navbar = $('.sticky');

            $(window).scrollTop($('.variance').offset().top - 20 - $navbar.height());

            return false
        }
    }

    return true
}